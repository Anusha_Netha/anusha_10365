import React from 'react';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import Home from './components/Home'
import DisplayEmployee from './components/displayEmployee';
import DisplayProduct from './components/displayProduct';
import AddProducts from './components/addProducts';
import productQuantity from './components/productIncreased';
import DeleteEmployee from './components/deleteEmployee';
import DeleteProduct from './components/deleteProduct';

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path={"/"} component={Home}/>
            <Route path={'/displayEmployees'} component={DisplayEmployee}/>
            <Route path={'/displayProducts'} component={DisplayProduct}/>
            <Route path={'/addProducts'} component={AddProducts}/>
            <Route path={'/increaseQuantity'} component={productQuantity}/>
            <Route path={'/deleteEmployee'} component={DeleteEmployee}/>
            <Route path={'/deleteProduct'} component={DeleteProduct}/>
        </Switch>
    </BrowserRouter>
)
export default Routes;