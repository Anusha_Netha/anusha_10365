import React, { Component } from 'react';
import './deleteProduct.css';

class deleteProduct extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            productname : '',
         }
    }
    handleChange = event => {
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log("id"+this.state.productname)
        const data = {
            'productname':this.state.productname,
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/delProducts",{
            method : 'DELETE',
            // mode:'no-cors',
            // headers :{'content-Type' : 'application/json;charset=utf-8'},
            headers:{   "Accept": "application/json",'Content-Type' : 'application/json;charset=utf-8'},
            body : JSON.stringify(data)
            
        })
        .then((res) => res.json)
        .then((data) => alert('deleted successfully'))
        .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input type="text" name="productname" placeholder="Enter Product Name" value={this.state.productname} onChange={this.handleChange.bind(this)}/><br/>
                    <button>DELETE PRODUCT</button>
                </form>
            </div>
         );
    }
}
 
export default deleteProduct;