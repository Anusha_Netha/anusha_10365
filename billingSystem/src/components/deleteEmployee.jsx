import React, { Component } from 'react';
import './deleteEmployee.css'
class deleteEmployee extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            employeeId : '',
            role:''
         }
    }
    handleChange = event => {
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log("id"+this.state.employeeId)
        console.log('role'+this.state.role)
        const data = {
            'employeeId':this.state.employeeId,
            'role':this.state.role,
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/delEmployee",{
            method : 'DELETE',
            // mode:'no-cors',
            // headers :{'content-Type' : 'application/json;charset=utf-8'},
            headers:{   "Accept": "application/json",'Content-Type' : 'application/json;charset=utf-8'},
            body : JSON.stringify(data)
            
        })
        .then((res) => res.json)
        .then((data) => alert('deleted successfully'))
        .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input type="text" name="employeeId" placeholder="Enter Employee ID" value={this.state.employeeId} onChange={this.handleChange.bind(this)}/><br/>
                    <input type="text" name="role" placeholder="Enter Employee Role" value={this.state.role} onChange={this.handleChange.bind(this)}/><br/>
                    <button>DELETE EMPLOYEE</button>
                </form>
            </div>
         );
    }
}
 
export default deleteEmployee;