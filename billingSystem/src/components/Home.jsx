import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './Home.css';

class Home extends Component {
    render() { 
        return ( 
            <div>
                <div>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSoiMPN24PbpEiN35iCin8XRo79OGlRbgwsqkcnMX5pAjBZfp4p" 
                    alt="supermarket" height="80px" width="150px"/>
                    <h2 style={{float:'right',marginRight:'30px',marginTop:'40px'}}>Innominds Quality Store</h2>
                </div>
                <nav className="navbar navbar-expand-sm">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <Link to="/displayEmployees" className="nav-link">Display Employees</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/displayProducts" className="nav-link">Display Products</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/addProducts" className="nav-link">Add a Product</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/increaseQuantity" className="nav-link">increase Quantity</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/deleteEmployee" className="nav-link">Delete Employee</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/deleteProduct" className="nav-link">Delete Product</Link>
                        </li>
                    </ul>
                </nav>
            </div>
         );
    }
}
 
export default Home;

