import React, { Component } from 'react';
import './displayProduct.css'

class DisplayProduct extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            products : [ ],
            isLoaded : false
         }
    }
    componentDidMount(){
        fetch(' http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
        .then(res => res.json())
        .then(data => {
            this.setState({
                isLoaded : true,
                products : data
            })
        });
    }
    render() { 
        var {isLoaded,products} = this.state;
        if(!isLoaded){
            return <div>Loading...</div>
        }
        return ( 
            <div className="App">
                <table>
                    <tr><th>ID</th><th>Name</th><th>Price</th><th>Quantity</th><th>Product Type</th></tr>
                    {products.map(product =>(
                        <tr key={product.product_id}>
                            <td>{product.product_id}</td>
                            <td>{product.productname}</td>
                            <td>{product.price}</td>
                            <td>{product.quantity}</td>
                            <td>{product.productType}</td>
                        </tr>
                    ))}
                </table>
            </div>
         );
    }
}
 
export default DisplayProduct;