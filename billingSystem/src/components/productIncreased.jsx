import React, { Component } from 'react';
import './productIncreased.css';

class productQuantity extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            productname : '',
            quantity :'',
         }
    }
    handleChange = event => {
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log("name"+this.state.productname);
        console.log("quantity"+this.state.quantity);
        const data = {
            'productname':this.state.productname,
            'quantity':this.state.quantity,
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/increaseQuantity",{
            method : 'PUT',
            // mode:'no-cors',
            // headers :{'content-Type' : 'application/json;charset=utf-8'},
            headers:{   "Accept": "application/json",'Content-Type' : 'application/json;charset=utf-8'},
            body : JSON.stringify(data)
            
        })
        .then((res) => res.json)
        .then((data) => alert('updated successfully'))
        .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input type="text" name="productname" placeholder="Enter Product Name" value={this.state.productname} onChange={this.handleChange.bind(this)}/><br/>
                    <input type="text" name="quantity" placeholder="Enter Product Quantity" value={this.state.quantity}  onChange={this.handleChange.bind(this)}/><br/>
                    <button>Increase Quantity</button>
                </form>
            </div>
         );
    }
}
 
export default productQuantity;