import React, { Component } from 'react';
import './displayEmployee.css'

class DisplayEmployee extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            employees : [ ],
            isLoaded : false
         }
    }
    componentDidMount(){
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/displayEmployee')
        
        .then(res => res.json())
        .then(data => {
            this.setState({
                isLoaded : true,
                employees : data
            })
        });
    }
    render() { 
        var {isLoaded,employees} = this.state;
        if(!isLoaded){
            return <div>Loading...</div>
        }
        return ( 
            <div className="App">
                <table>
                    <tr><th>ID</th><th>Name</th><th>Password</th><th>Role</th><th>Security Answer</th></tr>
                        {employees.map(employee => (
                            <tr key={employee.employeeId}>
                                <td>{employee.employeeId}</td>
                                <td>{employee.employeeName}</td>
                                <td>{employee.employeePassword}</td>
                                <td>{employee.role}</td>
                                <td>{employee.securityQuestion}</td>
                            </tr>
                        ))}
                   
                </table>
            </div>
         );
    }
}
 
export default DisplayEmployee;