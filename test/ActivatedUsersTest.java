package com.MobileBillNew.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.service.ActivatedUsers;

/**
 * this class is used to test the activated users
 * @author IMVIZAG
 *
 */
public class ActivatedUsersTest {
	
	ActivatedUsers user = null;
	
	/**
	 * this method is used to start test case
	 */
	@Before
	public void setUp() {
		user = new ActivatedUsers();	
	}
	
	/**
	 * this method is used to end the test case
	 */
	@After
	public void tearDown() {
		user = null;
	}
	
	/**
	 * this method is used to test plans activation
	 */
	@Test
	public void plansActivationTest() {
		Adminplans plans = new Adminplans();
		plans.setAmazon_prime("28days");
		plans.setAmount(199);
		plans.setCalls(100);
		plans.setData("unlimited");
		plans.setNetfix("28days");
		plans.setPackType("special plan");
		plans.setPlan_name("RC199");
		plans.setValidity(28);
		Date activation_date = new Date(2019-01-22);
		Date expiry_date = new Date(2019-02-21);
		boolean activation = user.plansActivation(plans, activation_date, expiry_date, 1);
		assertTrue(activation);
	}
	
	/**
	 * this method is used to test plans activation in negative case
	 */
	@Test
	public void plansActivationNegativeTest() {
		Adminplans plans = new Adminplans();
		plans.setAmazon_prime("28days");
		plans.setAmount(199);
		plans.setCalls(100);
		plans.setData("unlimited");
		plans.setNetfix("28days");
		plans.setPackType("special plan");
		plans.setPlan_name("RC199");
		plans.setValidity(28);
		Date activation_date = new Date(2019-01-22);
		Date expiry_date = new Date(2019-02-21);
		boolean activation = user.plansActivation(plans, activation_date, expiry_date, 0);
		assertFalse(activation);
	}
	
	/**
	 * this method is used to test to get the user id 
	 */
	@Test
	public void getUserIdTest() {
		int user_id = user.getUsersId(1);
		assertNotNull(user_id);
	}
	
	/**
	 * this method is used to test to get the user id in negative case
	 */
	@Test
	public void getUserIdNegativeTest() {
		int user_id = user.getUsersId(0);
		assertEquals(0,user_id);
	}

	/**
	 * this method is used to test to get the actiavtion id
	 */
	@Test
	public void getActivationIdTest() {
		int activation_id = user.getActivationId(1);
		assertNotNull(activation_id);
	}
	
	/**
	 * this method is used to test to get the actiavtion id in negative case
	 */
	@Test
	public void getActivationIdNegativeTest() {
		int activation_id = user.getActivationId(0);
		assertEquals(0,activation_id);
	}
}
