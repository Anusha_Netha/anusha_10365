const path = require('path');


module.exports = {
    entry : './src/index.js',
    output : {
        path : path.join(__dirname,'/dist'),
        filename : 'index_bundle.js'
    },
    devServer:{
        contentBase:"./public",
        hot : true
    },
    module : {
        rules : [
            {
                test : /\.js$/,
                exclude : /node_modules/,
                use: {
                    loader : "babel-loader"
                }
            }
        ]
    }
}