package com.jsonproject.dao;

import java.util.List;

import com.jsonproject.bean.Student;


/**
 * This is StudentInterface and it contains methods which connects to database and will be implemented in other class
 * @author IMVIZAG
 *
 */
public interface StudentDAO {
	
	/**
	 * This method is used to insert a student into database
	 */
	boolean createStudent( Student student );
	
	/**
	 * this method is used to search a student by their id
	 */
	Student searchById(int id);
	
	/**
	 * this method is used to display all the students in the database
	 */
	List<Student> getAllStudents();

	/**
	 * this method is used to update a student's name by their id
	 */
	boolean changeStudent(int id,String name);
	
	/**
	 * this method is used to delete a student by their id 
	 */
	boolean deleteById(int id);
}
