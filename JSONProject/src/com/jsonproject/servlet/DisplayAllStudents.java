package com.jsonproject.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jsonproject.bean.Student;
import com.jsonproject.service.StudentService;
import com.jsonproject.service.StudentServiceImpl;
import com.jsonproject.util.JsonConverter;



	/**
	 * Servlet implementation class DisplayAllStudents
	 */
	@WebServlet("/DisplayAllStudents")
	public class DisplayAllStudents extends HttpServlet {
		private static final long serialVersionUID = 1L;
	       
	   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   	
	   		@SuppressWarnings("unused")
			HttpSession session = request.getSession();
	   		StudentService service = new StudentServiceImpl();
	   		List<Student> studentList = service.fetchAllStudents();
	   		
	   		PrintWriter pw = response.getWriter();
	   		JsonConverter jsonConverter = new JsonConverter();
	   		String result = jsonConverter.convertToJson(studentList);
	   		request.setAttribute("result",result);
	   		pw.println(result);
	   		pw.close();
	   			   		
//	   		RequestDispatcher rd = request.getRequestDispatcher("Display.html");
//	   		rd.forward(request, response);
//	   		request.setAttribute("studentList", studentList);
//	   		if(studentList.isEmpty()) {
//	   			response.sendRedirect("Error1.html");
//	   		}
//	   		else {
//	   			rd.forward(request, response);
//	   		}
//	   		Iterator<Student> i= studentList.iterator();
//	   		
//	   		PrintWriter out = response.getWriter();
//	   		out.print("<html><body bgcolor='#e8edf3'>");
//	   		if(studentList.isEmpty()) {
//	   			out.println("Sorry!!!We dont have records");
//	   		}
//	   		else {
//	   		out.println("<table border='1'>");
//	   		out.println("<tr><th>ID</th><th>Name</th><th>Age</th></tr>");
//	   		while(i.hasNext()) {
//	   			Student student=i.next();
//	   			out.println("<tr><td>"+student.getId()+"</td>");
//	   			out.println("<td>"+student.getName()+"</td>");
//	   			out.println("<td>"+student.getAge()+"</td></tr>");
//	   		}
//	   		out.println("</table>");
//	   		}
//	   		out.print("</body></html>");
//			out.close();
	   	
	   		
	   		
	   		
	   	}

	}

