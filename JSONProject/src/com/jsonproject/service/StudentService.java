package com.jsonproject.service;

import java.util.List;

import com.jsonproject.bean.Student;


/**
 * this interface is used to provide services to access the database
 * @author IMVIZAG
 *
 */
public interface StudentService {
	
	/**
	 * this method is used to insert a student 
	 * @param student
	 * @return
	 */
	boolean insertStudent( Student student );
	
	/**
	 * this method is used to find student by their id
	 * @param id
	 * @return
	 */
	Student findById(int id);
	
	/**
	 * this method is used to display all the students 
	 * @return
	 */
	List<Student> fetchAllStudents();
	
	/**
	 * this method is used to update a student's name their id
	 * @param id
	 * @param name
	 * @return
	 */
	boolean updateStudent(int id,String name);
	
	/**
	 * this method is used to delete a student by their id
	 * @param id
	 * @return
	 */
	boolean deleteById(int id);
}
