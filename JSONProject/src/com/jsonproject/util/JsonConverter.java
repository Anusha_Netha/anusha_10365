package com.jsonproject.util;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jsonproject.bean.Student;


public class JsonConverter {

private final Gson gson;
    
    public JsonConverter() {
        
        gson = new GsonBuilder().create();
    }

    public String convertToJson(List<Student> studentList) {
        
        JsonArray jarray = gson.toJsonTree(studentList).getAsJsonArray();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("studentList", jarray);

        return jsonObject.toString();
    }
	
	
}













