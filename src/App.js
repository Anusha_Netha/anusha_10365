import React, { Component } from 'react';
import './App.css';
import Home from './pages/Home.js';
import {Route} from 'react-router-dom';
import LearnHtml from './pages/LearnHtml.js';

class App extends Component {
  render() {
    return (
      <div>
        <div>
          <Route path="/" exact component={Home}></Route>
          <Route path="/html" component={LearnHtml}></Route>
        </div>
      </div>
    );
  }
}

export default App;
