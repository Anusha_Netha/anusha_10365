import React from 'react';

export default function LearnHtmlSideNav() {
    return (
        <div class="cols-lg-3" style={{ marginLeft: '20px' }}>
            <div class="sidebar" style={{ height: '680px', overflow: 'auto', position: 'fixed' }}>
                <h3>HTML5 Tutorial</h3>
                <li style={{ listStyleType: 'none' }}>HTML HOME</li>
                <li style={{ listStyleType: 'none' }}>HTML Introduction</li>
                <li style={{ listStyleType: 'none' }}>HTML Editors</li>
                <li style={{ listStyleType: 'none' }}>HTML Basic</li>
                <li style={{ listStyleType: 'none' }}>HTML Elements</li>
                <li style={{ listStyleType: 'none' }}>HTML Attributes</li>
                <li style={{ listStyleType: 'none' }}>HTML Headings</li>
                <li style={{ listStyleType: 'none' }}>HTML Paragraphs</li>
                <li style={{ listStyleType: 'none' }}>HTML Styles</li>
                <li style={{ listStyleType: 'none' }}>HTML Formatting</li>
                <li style={{ listStyleType: 'none' }}>HTML Quotations</li>
                <li style={{ listStyleType: 'none' }}>HTML Comments</li>
                <li style={{ listStyleType: 'none' }}>HTML Colors</li>
                <li style={{ listStyleType: 'none' }}>HTML CSS</li>
                <li style={{ listStyleType: 'none' }}>HTML Links</li>
                <li style={{ listStyleType: 'none' }}>HTML Images</li>
                <li style={{ listStyleType: 'none' }}>HTML Tables</li>
                <li style={{ listStyleType: 'none' }}>HTML Lists</li>
                <li style={{ listStyleType: 'none' }}>HTML Blocks</li>
                <li style={{ listStyleType: 'none' }}>HTML Classes</li>
                <li style={{ listStyleType: 'none' }}>HTML ID</li>
                <li style={{ listStyleType: 'none' }}>HTML Iframes</li>
                <li style={{ listStyleType: 'none' }}>HTML JavaScript</li>
                <li style={{ listStyleType: 'none' }}>HTML File Paths</li>
                <li style={{ listStyleType: 'none' }}>HTML Head</li>
                <li style={{ listStyleType: 'none' }}>HTML Layout</li>
                <li style={{ listStyleType: 'none' }}>HTML Responsive</li>
                <li style={{ listStyleType: 'none' }}>HTML Computercode</li>
                <li style={{ listStyleType: 'none' }}>HTML Entities</li>
                <li style={{ listStyleType: 'none' }}>HTML Symbols</li>
                <li style={{ listStyleType: 'none' }}>HTML Charset</li>
                <li style={{ listStyleType: 'none' }}>HTML URL Encode</li>
                <li style={{ listStyleType: 'none' }}>HTML XHTML</li>
                <h3>HTML FORMS</h3>
                <li style={{ listStyleType: 'none' }}>HTML Forms</li>
                <li style={{ listStyleType: 'none' }}>HTML Form Elements</li>
                <li style={{ listStyleType: 'none' }}>HTML Input Types</li>
                <li style={{ listStyleType: 'none' }}>HTML Input Attributes</li>
                <h3>HTML5</h3>
                <li style={{ listStyleType: 'none' }}>HTML5 Intro</li>
                <li style={{ listStyleType: 'none' }}>HTML5 Support</li>
                <li style={{ listStyleType: 'none' }}>HTML5 New Elements</li>
                <li style={{ listStyleType: 'none' }}>HTML5 Semantics</li>
                <li style={{ listStyleType: 'none' }}>HTML5 Migration</li>
                <li style={{ listStyleType: 'none' }}>HTML5 Style Guide</li>
                <h3>HTML Graphics</h3>
                <li style={{ listStyleType: 'none' }}>HTML Canvas</li>
                <li style={{ listStyleType: 'none' }}>HTML SVG</li>
                <h3>HTML Media</h3>
                <li style={{ listStyleType: 'none' }}>HTML Media</li>
                <li style={{ listStyleType: 'none' }}>HTML Video</li>
                <li style={{ listStyleType: 'none' }}>HTML Audio</li>
                <li style={{ listStyleType: 'none' }}>HTML Plug-ins</li>
                <li style={{ listStyleType: 'none' }}>HTML YouTube</li>
                <h3>HTML APIs</h3>
                <li style={{ listStyleType: 'none' }}>HTML Geolocation</li>
                <li style={{ listStyleType: 'none' }}>HTML Drag/Drop</li>
                <li style={{ listStyleType: 'none' }}>HTML Web Storage</li>
                <li style={{ listStyleType: 'none' }}>HTML Web Workers</li>
                <li style={{ listStyleType: 'none' }}>HTML SSE</li>
                <h3>HTML Examples</h3>
                <li style={{ listStyleType: 'none' }}>HTML Examples</li>
                <li style={{ listStyleType: 'none' }}>HTML Quiz</li>
                <li style={{ listStyleType: 'none' }}>HTML Exercises</li>
                <li style={{ listStyleType: 'none' }}>HTML Certificate</li>
                <li style={{ listStyleType: 'none' }}>HTML Summary</li>
                <li style={{ listStyleType: 'none' }}>HTML Accessibility</li>
                <h3>HTML References</h3>
                <li style={{ listStyleType: 'none' }}>HTML Tag List</li>
                <li style={{ listStyleType: 'none' }}>HTML Attributes</li>
                <li style={{ listStyleType: 'none' }}>HTML Events</li>
                <li style={{ listStyleType: 'none' }}>HTML Colors</li>
                <li style={{ listStyleType: 'none' }}>HTML Canvas</li>
                <li style={{ listStyleType: 'none' }}>HTML Audio/Video</li>
                <li style={{ listStyleType: 'none' }}>HTML Doctypes</li>
                <li style={{ listStyleType: 'none' }}>HTML Character Sets</li>
                <li style={{ listStyleType: 'none' }}>HTML URL Encode</li>
                <br /><br />
            </div>
        </div>
    );
}