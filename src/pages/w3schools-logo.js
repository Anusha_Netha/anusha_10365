import React from 'react';

export default function w3schools_logo(){
    return(
        <div style={{ margin: '4px 0 6px 0' }}><a class="w3schools-logo" href="#">w3schools<span class="dotcom">.com</span></a>
            <div style={{ position: 'relative', top: '12px', marginRight: '10px', float: 'right' }}>THE WORLD'S LARGEST WEB DEVELOPER SITE</div>
        </div>
    );
}