import React from 'react';

export default function HomeNav() {
    let listStyle = {
        listStyleType: 'none',
    }
    let background = {
        backgroundColor: '#4CAF50',
    }
    return (
        <nav className="navbar navbar-expand-sm " style={background}>
            <ul className="navbar-nav">
                <li className="nav-item dropdown" style={listStyle}>
                    <a id="tutorial" href="#" className="nav-link dropdown-toggle" style={{ fontSize: '18px', width: '130px' }} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tutorials</a>
                    <div class="dropdown-menu">
                        <h3 className="dropdown-item">HTML and CSS</h3>
                        <a className="dropdown-item" href="#">Learn HTML</a>
                        <a className="dropdown-item" href="#">Learn CSS</a>
                        <a className="dropdown-item" href="#">Learn W3.Colors</a>
                        <a className="dropdown-item" href="#">Learn Bootstrap 3</a>
                        <a className="dropdown-item" href="#">Learn Bootstrap 4</a>
                        <a className="dropdown-item" href="#">Learn Icons</a>
                        <a className="dropdown-item" href="#">Learn Graphics</a>
                        <a className="dropdown-item" href="#">Learn SVG</a>
                        <a className="dropdown-item" href="#">Learn Canvas</a>
                        <a className="dropdown-item" href="#">Learn How To</a>
                        <h3 className="dropdown-item">Javascript</h3>
                        <a className="dropdown-item" href="#">Learn Javascript</a>
                        <a className="dropdown-item" href="#">Learn jQuery</a>
                        <a className="dropdown-item" href="#">Learn AngularJS</a>
                        <a className="dropdown-item" href="#">Learn JSON</a>
                        <a className="dropdown-item" href="#">Learn AJAX</a>
                        <a className="dropdown-item" href="#">Learn W3.JS</a>
                        <h3 className="dropdown-item">Server Side</h3>
                        <a className="dropdown-item" href="#">Learn SQL</a>
                        <a className="dropdown-item" href="#">Learn PHP</a>
                        <a className="dropdown-item" href="#">Learn Python</a>
                        <a className="dropdown-item" href="#">Learn Java</a>
                        <a className="dropdown-item" href="#">Learn ASP</a>
                        <a className="dropdown-item" href="#">Learn Node.js</a>
                        <a className="dropdown-item" href="#">Learn Raspberry Pi</a>
                        <h3 className="dropdown-item">Web Building</h3>
                        <a className="dropdown-item" href="#">Web Templates</a>
                        <a className="dropdown-item" href="#">Web Statistics</a>
                        <a className="dropdown-item" href="#">Web Certificates</a>
                        <a className="dropdown-item" href="#">Web Editor</a>
                        <a className="dropdown-item" href="#">Web Development</a>
                        <h3 className="dropdown-item">XML Tutorials</h3>
                        <a className="dropdown-item" href="#">Learn XML</a>
                        <a className="dropdown-item" href="#">Learn XML AJAX</a>
                        <a className="dropdown-item" href="#">Learn XML DOM</a>
                        <a className="dropdown-item" href="#">Learn XML DTD</a>
                        <a className="dropdown-item" href="#">Learn XML Schema</a>
                        <a className="dropdown-item" href="#">Learn XSLT</a>
                        <a className="dropdown-item" href="#">Learn XPath</a>
                        <a className="dropdown-item" href="#">Learn XQuery</a>
                    </div>
                </li>
                <li className="nav-item dropdown" style={listStyle}>
                    <a href="#" className="nav-link dropdown-toggle" style={{ fontSize: '18px', width: '150px' }} id="references" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">References</a>
                    <div className="dropdown-menu">
                        <h3 className="dropdown-item">HTML</h3>
                        <a className="dropdown-item" href="#">HTML Tag Reference</a>
                        <a className="dropdown-item" href="#">HTML Event Reference</a>
                        <a className="dropdown-item" href="#">HTML Color Reference</a>
                        <a className="dropdown-item" href="#">HTML Attribute Reference</a>
                        <a className="dropdown-item" href="#">HTML Canvas Reference</a>
                        <a className="dropdown-item" href="#">HTML SVG Reference</a>
                        <a className="dropdown-item" href="#">Google Maps Reference</a>
                        <h3 className="dropdown-item" >CSS</h3>
                        <a className="dropdown-item" href="#">CSS Reference</a>
                        <a className="dropdown-item" href="#">CSS Browser Reference</a>
                        <a className="dropdown-item" href="#">CSS Selector Reference</a>
                        <a className="dropdown-item" href="#">W3.CSS Reference</a>
                        <a className="dropdown-item" href="#">BootStrap Reference</a>
                        <a className="dropdown-item" href="#">Icon Reference</a>
                        <h3 className="dropdown-item">Javascript</h3>
                        <a className="dropdown-item" href="#">Javascript Reference</a>
                        <a className="dropdown-item" href="#">HTML DOM Reference</a>
                        <a className="dropdown-item" href="#">jQuery Reference</a>
                        <a class="dropdown-item" href="#">AngularJS Reference</a>
                        <a class="dropdown-item" href="#">W3.JS Reference</a>
                        <h3 class="dropdown-item">Server Side</h3>
                        <a class="dropdown-item" href="#">SQL Reference</a>
                        <a class="dropdown-item" href="#">PHP Reference</a>
                        <a class="dropdown-item" href="#">Python Reference</a>
                        <a class="dropdown-item" href="#">ASP Reference</a>
                        <h3 class="dropdown-item" href="#">XML</h3>
                        <a class="dropdown-item" href="#">XML DOM Reference</a>
                        <a class="dropdown-item" href="#">XML Http Reference</a>
                        <a class="dropdown-item" href="#">XSLT Reference</a>
                        <a class="dropdown-item" href="#">XML Schema Reference</a>
                        <h3 class="dropdown-item">Character Sets</h3>
                        <a class="dropdown-item" href="#">HTML Character Sets</a>
                        <a class="dropdown-item" href="#">HTML ASCII</a>
                        <a class="dropdown-item" href="#">HTML ANSI</a>
                        <a class="dropdown-item" href="#">HTML Windows-1252</a>
                        <a class="dropdown-item" href="#">HTML ISO-8859-1</a>
                        <a class="dropdown-item" href="#">HTML Symbols</a>
                        <a class="dropdown-item" href="#">HTML UTF-8</a>
                    </div>
                </li>
                <li className="nav-item dropdown" style={listStyle}>
                    <a href="#" className="nav-link dropdown-toggle" style={{ fontSize: '18px', width: '150px' }} id="examples" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Examples</a>
                    <div class="dropdown-menu">
                        <h3 class="dropdown-item">HTML and CSS</h3>
                        <a class="dropdown-item" href="#">HTML Examples</a>
                        <a class="dropdown-item" href="#">HTML Exercises</a>
                        <a class="dropdown-item" href="#">CSS Examples</a>
                        <a class="dropdown-item" href="#">CSS Exercises</a>
                        <a class="dropdown-item" href="#">W3.CSS Examples</a>
                        <a class="dropdown-item" href="#">W3.CSS Templates</a>
                        <a class="dropdown-item" href="#">Bootstrap Examples</a>
                        <a class="dropdown-item" href="#">How To Examples</a>
                        <a class="dropdown-item" href="#">SVG Examples</a>
                        <h3 class="dropdown-item">Javascript</h3>
                        <a class="dropdown-item" href="#">Javascript Examples</a>
                        <a class="dropdown-item" href="#">Javascript Exercises</a>
                        <a class="dropdown-item" href="#">HTML DOM Examples</a>
                        <a class="dropdown-item" href="#">jQuery Examples</a>
                        <a class="dropdown-item" href="#">AngularJS Examples</a>
                        <a class="dropdown-item" href="#">AJAX Examples</a>
                        <a class="dropdown-item" href="#">W3.JS Examples</a>
                        <h3 class="dropdown-item">Server Side</h3>
                        <a class="dropdown-item" href="#">SQL Exercises</a>
                        <a class="dropdown-item" href="#">PHP Examples</a>
                        <a class="dropdown-item" href="#">Python Exercises</a>
                        <a class="dropdown-item" href="#">ASP Examples</a>
                        <h3 class="dropdown-item">XML</h3>
                        <a class="dropdown-item" href="#">XML Examples</a>
                        <a class="dropdown-item" href="#">XSLT Examples</a>
                        <a class="dropdown-item" href="#">XPath Examples</a>
                        <a class="dropdown-item" href="#">XML Schema Examples</a>
                        <a class="dropdown-item" href="#">AJAX Examples</a>
                        <a class="dropdown-item" href="#">SVG Examples</a>
                        <h3 class="dropdown-item">Quizzes</h3>
                        <a class="dropdown-item" href="#">HTML Quiz</a>
                        <a class="dropdown-item" href="#">CSS Quiz</a>
                        <a class="dropdown-item" href="#">Javascript Quiz</a>
                        <a class="dropdown-item" href="#">Bootstrap Quiz</a>
                        <a class="dropdown-item" href="#">jQuery quiz</a>
                        <a class="dropdown-item" href="#">PHP Quiz</a>
                        <a class="dropdown-item" href="#">SQl Quiz</a>
                        <a class="dropdown-item" href="#">XML Quiz</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav" style={{ marginLeft: '700px' }}>
                <li class="nav-item" style={{ listStyleType: 'none', paddingTop: '6px', paddingBottom: '5px' }} ><a href="#" id="globe"><i class="nav-link fa fa-globe fa-x" ></i></a></li>
                <li class="nav-item" style={{ listStyleType: 'none', paddingTop: '6px', paddingBottom: '5px', marginLeft: '10px' }} ><a href="#" id="search"><i class="nav-link fa fa-search fa-x" ></i></a></li>
            </ul>
        </nav>
    );
}