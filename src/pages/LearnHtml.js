import React, { Component } from 'react';
import './Styles.css';
import W3schools_Logo from './w3schools-logo';
import LearnHtmlNav from './LearnHtmlNav.js';
import LearnHtmlSideNav from './LearnHtmlSideNav.js';
import LearnHtmlBody from './LearnHtmlBody.js';

class LearnHtml extends Component {

    render() {

        return (
            <div id="main" className="container-fluid">
                <div style={{ marginTop: '10px', fontFamily: 'bold' }}>
                    <W3schools_Logo />
                </div>
                <div>
                    <LearnHtmlNav />
                </div>
                <div className="row">
                    <div>
                        <LearnHtmlSideNav />
                    </div>
                    <div>
                        <LearnHtmlBody />
                    </div>
                </div>
            </div>
        );
    }

}

export default LearnHtml;