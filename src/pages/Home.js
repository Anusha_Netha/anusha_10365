import React, { Component } from 'react';
import './Styles.css';
import W3schools_Logo from './w3schools-logo.js';
import HomeNav from './HomeNav';
import HomeSideNav from './HomeSideNav.js';
import HomeBody from './HomeBody.js';

class Home extends Component {

    render() {

        return (
            <div id="main" className="container-fluid">
                <div style={{ marginTop: '10px', fontFamily: 'bold' }}>
                    <W3schools_Logo />
                </div>
                <div><HomeNav /></div>
                <div className="row">
                    <div><HomeSideNav /></div>
                    <div><HomeBody /></div>
                </div>
            </div>
        );
    }
}

export default Home;