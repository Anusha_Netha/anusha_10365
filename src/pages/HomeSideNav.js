import React from 'react';

export default function HomeSideNav(){
    return (
        <div class="cols-lg-3" style={{ marginLeft: "20px", }}>
            <li style={{ listStyleType: 'none', fontSize: "26px", amrginTop: '20px' }}>HTML and CSS</li>
            <li style={{ listStyleType: 'none', fontSize: "18px", marginTop: '20px' }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn HTML</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn CSS</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn W3.CSS</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Colors</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Bootstrap 3</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Bootstrap 4</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Icons</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Graphics</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn How To</a></li>
            <li style={{ listStyleType: 'none', fontSize: "26px", marginTop: '20px' }}>Javascript</li>
            <li style={{ listStyleType: 'none', fontSize: "18px", marginTop: '20px' }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold', }}>Learn Javascript</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn jQuery</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn AngularJS</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn JSON</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn AJAX</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn W3.JS</a></li>
            <li style={{ listStyleType: 'none', fontSize: "26px", marginTop: '20px' }}>Server Side</li>
            <li style={{ listStyleType: 'none', fontSize: "18px", marginTop: '20px' }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold', }}>Learn SQL</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn PHP</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Python</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Java</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn ASP</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Node.js</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn Raspberry Pi</a></li>
            <li style={{ listStyleType: 'none', fontSize: "26px", marginTop: '20px' }}>Web Building</li>
            <li style={{ listStyleType: 'none', fontSize: "18px", marginTop: '20px' }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold', }}>Web Templates</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Web Statistics</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Web Certificates</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Web Editor</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Web Development</a></li>
            <li style={{ listStyleType: 'none', fontSize: "26px", marginTop: '20px' }}>XML Tutorials</li>
            <li style={{ listStyleType: 'none', fontSize: "18px", marginTop: '20px' }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold', }}>Learn XML</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn XML AJAX</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn XML DOM</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn XML DTD</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn XML Schema</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn XSLT</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn XPath</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Learn XQuery</a></li>
            <li style={{ listStyleType: 'none', fontSize: "26px", marginTop: '20px' }}>References</li>
            <li style={{ listStyleType: 'none', fontSize: "18px", marginTop: '20px' }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold', }}>HTML Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>CSS Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>JS Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>SQL Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>PHP Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>jQuery Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Python Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Bootstrap Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>W3.CSS Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>UTF-8 Reference</a></li>
            <li style={{ listStyleType: 'none', fontSize: "26px", marginTop: '20px' }}>Exercises</li>
            <li style={{ listStyleType: 'none', fontSize: "18px", marginTop: '20px' }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold', }}>HTML Exercises</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>CSS Exercises</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Javascript Exercises</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>SQL Exercises</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>PHP Exercises</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Python Exercises</a></li>
            <li style={{ listStyleType: 'none', fontSize: "18px" }}><a href="#" style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }}>Bootstrap Exercises</a></li>
        </div>
    );
}