import React from 'react';

export default function LearnHtmlBody() {
    return (
        <div class="cols-lg-9" style={{ marginLeft: "250px" }}>
            <div class='row' style={{ marginLeft: '20px' }}>
                <div class='cols-lg-9' style={{ height: '100px', width: '100%', }}>
                </div>
            </div>
            <div class="row" style={{ marginLeft: '20px' }}>
                <div class='cols-lg-9'>
                    <h2>HTML5 Tutorial</h2>
                    <button style={{ backgroundColor: 'green', color: 'white', border: 'none', height: '40px', width: '100px', marginTop: '20px' }}>&lt;HOME</button>
                    <button style={{ backgroundColor: 'green', color: 'white', border: 'none', height: '40px', width: '100px', marginTop: '20px', marginLeft: '500px' }}>NEXT&gt;</button>
                    <div style={{ backgroundColor: '#adebad', height: '150px', width: '100%', marginTop: '20px' }}><br />
                        <p style={{ marginLeft: '20px' }}><b>With HTML you can create your own Website.</b></p>
                        <p style={{ marginLeft: '20px' }}><b>This Tutorial teaches you everything about HTML.</b></p>
                        <p style={{ marginLeft: '20px' }}  ><b>HTML is easy to learn - You will enjoy it</b></p>
                    </div>
                    <hr />
                </div>
                <div class="cols-lg-3"></div>
            </div>
            <div class="row">
                <div class="cols-lg-9" style={{ marginLeft: '30px', width: '700px' }}>
                    <br />
                    <h3>Examples in Every Chapter</h3>
                    <br />
                    <p>This HTML tutorial contains hundreds of HTML examples.</p>
                    <p>With our online HTML editor, you can edit the HTML, and click on a button to view the result.</p>
                    <div style={{ backgroundColor: '#f2f2f2', height: '420px', width: '100%' }}><br />
                        <h4 style={{ marginLeft: '20px' }}>Example</h4>
                        <div style={{ marginLeft: '20px', height: '280px', marginTop: '20px', backgroundColor: 'white', marginRight: '20px', }}><br />
                            <div style={{ marginLeft: '20px' }}>
                                &lt;!DOCTYPE html&gt;<br />
                                &lt;html&gt;<br />
                                &lt;head&gt;<br />
                                &lt;title&gt;Page Title&lt;/title&gt;<br />
                                &lt;/head&gt;<br /><br />
                                &lt;body&gt;<br />
                                &lt;h1&gt;This is Heading Tag&lt;/h1&gt;<br />
                                &lt;p&gt;This is Paragraph Tag&lt;/p&gt;<br />
                                &lt;/body&gt;
                                &lt;/html&gt;
                                       </div>
                        </div>
                        <button style={{ marginLeft: '20px', backgroundColor: 'green', color: 'white', border: 'none', height: '40px', width: '200px', marginTop: '20px' }}>TRY IT YOURSELF>></button>
                    </div>
                    <br />
                    <h5>click on the "Try it Yourself" button to see how it works.</h5>
                    <a href="#" style={{ textDecoration: 'underline', color: 'black', fontSize: '18px' }}>Start learning HTML now!</a>
                    <hr />
                </div>
                <div class="cols-lg-3"></div>
            </div>
            <div class="row">
                <div class="cols-lg-9" style={{ marginLeft: '30px', width: '700px' }}>
                    <h3>HTML EXAMPLES</h3>
                    <p>At the end of the HTML tutorial, you can find more than 200 examples.</p>
                    <p>With our online editor, you can edit and test each example yourself.</p>
                    <a href="#" style={{ fontSize: '18px', color: 'black', textDecoration: 'underline' }}>Go to HTML Examples</a>
                    <hr />
                </div>
                <div class="cols-lg-3"></div>
            </div>
            <div class="row">
                <div class="cols-lg-9" style={{ height: '100px', width: '700px', marginLeft: '30px', }}>
                </div>
            </div>
            <div class="row">
                <div class="cols-lg-9" style={{ width: '700px', marginLeft: '30px' }}>
                    <hr />
                    <h3>HTML Exercises and Quiz Test</h3><br />
                    <p style={{ fontSize: '18px' }}>Test your HTML skills at W3Schools!</p>
                    <p><a href="#" style={{ textDecoration: 'underline', fontSize: '18px', color: 'black' }}>Start HTML Exercises!</a></p>
                    <p><a href="#" style={{ textDecoration: 'underline', fontSize: '18px', color: 'black' }}>Start HTML Quiz!</a></p>
                    <hr />
                </div>
                <div class="cols-lg-9"></div>
            </div>
            <div class="row">
                <div class="cols-lg-9" style={{ marginLeft: '30px' }}>
                    <br /><h3>HTML References</h3><br />
                    <p style={{ fontSize: '16px' }}>At W3Schools you will find complete References about tags, attributes, events, color names,entities,<br />
                        character-sets, URL encoding, language codes, HTTP messages, and more.</p>
                    <a href="#" style={{ color: 'black', textDecoration: 'underline', fontSize: '16px' }}>HTML Tag Reference</a>
                    <hr />
                </div>
                <div class="cols-lg-3"></div>
            </div>
            <div class="row">
                <div class="cols-lg-9" style={{ marginLeft: '30px' }}>
                    <h3>HTML Exam - Get your Diploma</h3><br />
                    <table style={{ textAlign: 'left' }}>
                        <tr>
                            <td><img src="https://www.w3schools.com/images/w3cert.gif" alt="certification" height="100px" width="250px" /></td>
                            <td>
                                <h3>W3Schools' Online Certification</h3>
                                <p>The perfect solution for professionals who to need to work, family,<br /> and career building.</p>
                                <p>More than 10 000 certificates already issued!</p>
                            </td>
                        </tr>
                    </table>
                    <button style={{ backgroundColor: 'green', height: '40px', width: '200px', border: 'none', color: 'white' }}>Get your Certificate >></button><br /><br />
                    <p style={{ fontSize: '18px' }}>The <a href="#" style={{ textDecoration: 'underline', color: 'black' }}>HTML Certificate</a>&nbsp;documents your knowledge of HTML.</p>
                    <p style={{ fontSize: '18px' }}>The <a href="#" style={{ textDecoration: 'underline', color: 'black' }}>CSS Certificate</a>&nbsp;documents your knowledge of advanced CSS.</p>
                    <p style={{ fontSize: '18px' }}>The <a href="#" style={{ textDecoration: 'underline', color: 'black' }}>Javascript Certificate</a>&ndsp;documents your knowledge of Javascript and HTML DOM.</p>
                    <p style={{ fontSize: '18px' }}>The <a href="#" style={{ textDecoration: 'underline', color: 'black' }}>jQuery Certificate</a>&nbsp;documents your knowledge of jQuery.</p>
                    <p style={{ fontSize: '18px' }}>The <a href="#" style={{ textDecoration: 'underline', color: 'black' }}>PHP Certifiacte</a>&nbsp;documents your knowledge of PHP and SQL(MySQL).</p>
                    <p style={{ fontSize: '18px' }}>The <a href="#" style={{ textDecoration: 'underline', color: 'black' }}>XML Certificate</a>&nbsp;documents your knowledge of XML, XML DOM and XSLT.</p>
                    <p style={{ fontSize: '18px' }}>The <a href="#" style={{ textDecoration: 'underline', color: 'black' }}>Bootstrap Certificate</a>&nbsp;documents your knowledge of Bootstrap framework.</p>
                    <button style={{ backgroundColor: 'green', color: 'white', border: 'none', height: '40px', width: '100px', marginTop: '20px' }}>&lt;HOME</button>
                    <button style={{ backgroundColor: 'green', color: 'white', border: 'none', height: '40px', width: '100px', marginTop: '20px', marginLeft: '500px' }}>NEXT&gt;</button>
                </div>
            </div>
            <div class="row">
                <div class="cols-lg-12" style={{ width: '1024px', marginLeft: '30px', height: '400px' }}>
                    <hr />
                    <pre>              REPORT ERROR                      PRINT PAGE                          FORUM                          ABOUT</pre>
                    <hr />
                    <pre style={{ fontSize: '20px' }}>       Top 10 Tutorials       Top 10 References      Top 10 Examples      Web Certificates  </pre>
                    <pre>               <a href="#" style={{ fontsize: '14px', color: 'black' }}>HTML Tutorial</a>                    <a href="#" style={{ fontsize: '14px', color: 'black' }}>HTML Reference</a>                  <a href="#" style={{ fontsize: '14px', color: 'black' }}>HTML Examples</a>                <a href="#" style={{ fontsize: '14px', color: 'black' }}>HTML Certificate</a></pre>
                    <pre>               <a href="#" style={{ fontsize: '14px', color: 'black' }}>CSS Reference </a>                    <a href="#" style={{ fontsize: '14px', color: 'black' }}>CSS Reference</a>                   <a href="#" style={{ fontsize: '14px', color: 'black' }}>CSS Example</a>                  <a href="#" style={{ fontsize: '14px', color: 'black' }}>CSS Certificate</a> </pre>
                    <pre>            <a href="#" style={{ fontsize: '14px', color: 'black' }}>JavaScript Reference</a>              <a href="#" style={{ fontsize: '14px', color: 'black' }}>JavaScript Reference</a>           <a href="#" style={{ fontsize: '14px', color: 'black' }}>JavaScript Examples </a>          <a href="#" style={{ fontsize: '14px', color: 'black' }}>JavaScript Certificate</a> </pre>
                    <pre>             <a href="#" style={{ fontsize: '14px', color: 'black' }}>W3.CSS Reference </a>                  <a href="#" style={{ fontsize: '14px', color: 'black' }}>W3.CSS Reference</a>               <a href="#" style={{ fontsize: '14px', color: 'black' }}>How To Examples</a>               <a href="#" style={{ fontsize: '14px', color: 'black' }}>jQuery Certificate</a> </pre>
                    <pre>            <a href="#" style={{ fontsize: '14px', color: 'black' }}>Bootstrap Reference</a>               <a href="#" style={{ fontsize: '14px', color: 'black' }}>Bootstrap Reference </a>             <a href="#" style={{ fontsize: '14px', color: 'black' }}>W3.CSS Examples</a>                <a href="#" style={{ fontsize: '14px', color: 'black' }}> PHP Certificate</a> </pre>
                    <pre>              <a href="#" style={{ fontsize: '14px', color: 'black' }}>SQL Reference</a>                      <a href="#" style={{ fontsize: '14px', color: 'black' }}>SQL Reference</a>               <a href="#" style={{ fontsize: '14px', color: 'black' }}>Bootstrap Examples</a>             <a href="#" style={{ fontsize: '14px', color: 'black' }}>Bootstrap Certificate</a></pre>
                    <pre>              <a href="#" style={{ fontsize: '14px', color: 'black' }}>PHP Reference </a>                     <a href="#" style={{ fontsize: '14px', color: 'black' }}>PHP Reference</a>                  <a href="#" style={{ fontsize: '14px', color: 'black' }}>PHP Examples </a>                  <a href="#" style={{ fontsize: '14px', color: 'black' }}>XML Certificate</a></pre>
                    <pre>               <a href="#" style={{ fontsize: '14px', color: 'black' }}>HTML Colors </a>                       <a href="#" style={{ fontsize: '14px', color: 'black' }}>HTML Colors </a>                 <a href="#" style={{ fontsize: '14px', color: 'black' }}>jQuery Examples</a></pre>
                    <pre>            <a href="#" style={{ fontsize: '14px', color: 'black' }}> jQuery Reference</a>                  <a href="#" style={{ fontsize: '14px', color: 'black' }}>jQuery Reference </a>              <a href="#" style={{ fontsize: '14px', color: 'black' }}>Angular Examples</a> </pre>
                    <pre>             <a href="#" style={{ fontsize: '14px', color: 'black' }}>Python Reference</a>                  <a href="#" style={{ fontsize: '14px', color: 'black' }}>Python Reference</a>                 <a href="#" style={{ fontsize: '14px', color: 'black' }}>XML Examples</a> </pre>
                    <hr />
                    <div style={{ textAlign: 'center' }}>
                        <p>w3schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding. Tutorials, references,and examples are contantly reviewed o avoid errors, but e cannot warrant correctness of all content while using this site, you agree to have read and accepted our terms of use, cookie and privacy policy,<a href="#" style={{ color: 'black' }}>Copyright 1999-2019</a>by Refsnes Data. All Rights Reserved.</p>
                        <a href="#" style={{ color: 'black' }}>powered by W3.CSS</a><br /><br />
                        <img src="https://www.w3schools.com/images/w3schoolscom_gray.gif" alt="w3schools-logo" height="30px" width="200px" /><br /><br />
                    </div>
                </div>

            </div>
        </div>
    );
}