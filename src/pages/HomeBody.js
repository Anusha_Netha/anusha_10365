import React from 'react';
import { Link } from 'react-router-dom';

export default function HomeBody() {
    return (
        <div className="cols-lg-9" style={{ marginLeft: '60px' }}>
            <div className="row">
                <div className="cols-lg-6">
                    <div style={{ position: 'relative', marginTop: '20px', textAlign: 'center', marginLeft: '20px', width: '600px' }}>
                        <li style={{ listStyleType: 'none', fontSize: '80px', color: 'grey' }}>HTML</li>
                        <li style={{ listStyleType: 'none', color: 'grey', fontSize: '30px' }}>The language for building Web pages</li>
                        <button name="Learn HTML" style={{ border: 'none', height: '40px', backgroundColor: 'grey', width: '150px', marginTop: '20px' }}><Link to="/html" style={{ color: 'white' }}>LEARN HTML</Link></button>
                        <button name="HTML Reference" style={{ marginLeft: '20px', color: 'white', border: 'none', height: '40px', backgroundColor: 'grey', width: '150px', marginTop: '20px' }}>HTML References</button>
                    </div>
                </div>
                <div className="cols-lg-6" style={{ marginTop: '20px', marginLeft: '40px', backgroundColor: '#f2f2f2', height: '350px', width: '330px' }}>
                    <h3 style={{ marginLeft: '20px' }}>HTML Example</h3>
                    <div style={{ backgroundColor: 'white', height: '250px', width: '400px', marginLeft: '20px' }} >
                        &lt;!DOCTYPE html&gt;<br />
                        &lt;html&gt;<br />
                        &lt;head&gt;<br />
                        &lt;title&gt;HTML Tutorial&lt;/title&gt;<br />
                        &lt;/head&gt;<br />
                        &lt;body&gt;<br />
                        &lt;h1&gt;This is heading&lt;/h1&gt;<br />
                        &lt;p&gt;This is paragraph&lt;/p&gt;<br />
                        &lt;/body&gt;<br />
                        &lt;/html&gt;<br />
                    </div>
                    <button href='#' name="Try it Yourself" style={{ marginLeft: '20px', backgroundColor: 'green', marginTop: '10px', border: 'none', color: 'white', height: '40px' }}>TRY IT YOURSELF >></button>
                </div>

            </div>
            <div className="row" style={{ backgroundColor: '#f2f2f2', height: '500px', marginTop: '20px', marginLeft: '20px' }}>
                <div className="cols-lg-6" style={{ marginLeft: '20px', marginTop: '20px' }}>
                    <h3>CSS Example</h3>
                    <div style={{ backgroundColor: 'white', height: '300px', width: '400px', marginLeft: '20px' }}>
                        {/* body{<br/>
                                        background-color : 'lightblue';<br/>
                                    }<br/>
                                    h1{<br/>
                                        color:white;<br/>
                                        text-align:center;<br/>
                                    }<br/>
                                    p{
                                        <br/>
                                        font-size : 20px;<br/>
                                        font-family : verdana<br/>
                                    } */}
                    </div>
                    <button style={{ backgroundColor: 'green', height: '40px', marginLeft: '20px', width: '200px', border: 'none', color: 'white' }}>TRY IT YOURSELF>></button>
                </div>
                <div className="cols-lg-6" style={{ marginLeft: '100px', marginTop: '100px', textAlign: 'center' }}>
                    <li style={{ listStyleType: 'none', fontSize: '32px' }}>CSS</li>
                    <li style={{ listStyleType: 'none', fontSize: '18px' }}>The Language for Styling web Pages</li>
                    <button style={{ backgroundColor: 'green', height: '40px', width: '150px', border: 'none', color: 'white', marginTop: '30px' }}>LEARN CSS</button>
                    <button style={{ backgroundColor: 'green', height: '40px', width: '150px', border: 'none', marginLeft: '20px', color: 'white', marginTop: '30px' }}>CSS REFERENCES</button>
                </div>
            </div>
            <div className="row">
                <div className="cols-lg-6" style={{ marginTop: '20px', marginLeft: '30px' }}>
                    <li style={{ listStyleType: 'none', color: 'grey', fontSize: '50px', marginLeft: '30px' }}>JAVASCRIPT</li>
                    <li style={{ listStyleType: 'none', fontSize: '24px', color: 'grey' }}>The Language for programming web pages</li>
                    <button style={{ backgroundColor: 'green', height: '40px', width: '150px', border: 'none', color: 'white', marginTop: '30px' }}>LEARN JAVASCRIPT</button>
                    <button style={{ backgroundColor: 'green', height: '40px', width: '200px', border: 'none', color: 'white', marginTop: '30px', marginLeft: '20px' }}>JAVASCRIPT REFERENCE</button>
                </div>
                <div className="cols-lg-6" style={{ backgroundColor: '#f2f2f2', height: '400px', width: '500px', marginTop: '20px', marginLeft: '20px' }}>

                    <h4 style={{ marginTop: '20px', marginLeft: '20px' }}>JAVASCRIPT Example</h4>
                    <div style={{ backgroundColor: 'white', height: '250px', marginLeft: '20px' }}>
                        {/* &lt;button onClick="myFunction()"&gt;ClickMe&lt;/button&gt;<br/><br/>
                                    &lt;script&gt;<br/>
                                    function myFunction(){<br/>
                                        var x = document.getElementById("demo");
                                        x.style.fontSize = '25px';
                                        x.style.color = "red";
                                    } */}
                    </div>
                    <button style={{ marginLeft: '20px', backgroundColor: 'green', height: '40px', width: '200px', border: 'none', color: 'white', marginTop: '20px' }}>TRY IT YOURSELF>></button>
                </div>
            </div>
            <div className="row" style={{ backgroundColor: '#f2f2f2', height: '450px', marginTop: '20px' }}>
                <div className="cols-lg-6">
                    <div style={{ backgroundColor: 'white', marginLeft: '20px', marginTop: '30px', height: '180px', width: '480px', textAlign: 'center' }}>
                        <li style={{ listStyleType: 'none', fontSize: '30px' }}>SQL</li>
                        <li style={{ listStyleType: 'none', fontSize: '22px' }}>A langugage  for accessing database</li>
                        <button style={{ backgroundColor: 'grey', border: 'none', height: '40px', width: '100px', marginTop: '20px', color: 'white' }}>Learn SQL</button>
                    </div>
                    <div style={{ backgroundColor: 'white', marginLeft: '20px', marginTop: '30px', height: '180px', width: '480px', textAlign: 'center' }}>
                        <li style={{ listStyleType: 'none', fontSize: '30px' }}>jQuery</li>
                        <li style={{ listStyleType: 'none', fontSize: '22px' }}>A javascript library for developing web pages</li>
                        <button style={{ backgroundColor: 'grey', border: 'none', height: '40px', width: '150px', marginTop: '20px', color: 'white' }}>Learn JQUERY</button>
                    </div>
                </div>
                <div className="cols-lg-6">
                    <div style={{ backgroundColor: 'white', marginLeft: '20px', marginTop: '30px', height: '180px', width: '480px', textAlign: 'center' }}>
                        <li style={{ listStyleType: 'none', fontSize: '30px' }}>PHP</li>
                        <li style={{ listStyleType: 'none', fontSize: '22px' }}>A web server programming language</li>
                        <button style={{ backgroundColor: 'grey', border: 'none', height: '40px', width: '100px', marginTop: '20px', color: 'white' }}>Learn PHPL</button>
                    </div>
                    <div style={{ backgroundColor: 'white', marginLeft: '20px', marginTop: '30px', height: '180px', width: '480px', textAlign: 'center' }}>
                        <li style={{ listStyleType: 'none', fontSize: '30px' }}>Python</li>
                        <li style={{ listStyleType: 'none', fontSize: '22px' }}>A web server programming language</li>
                        <button style={{ backgroundColor: 'grey', border: 'none', height: '40px', width: '150px', marginTop: '20px', color: 'white' }}>Learn PYTHON</button>
                    </div>
                </div>
            </div>
            <div className="row" style={{ backgroundColor: 'white' }}>
                <div class="cols-lg-4">
                    <div style={{ textAlign: 'center', width: '300px', marginTop: '20px' }}>
                        <li style={{ listStyleType: 'none', fontSize: '24px' }}>W3.CSS</li>
                        <li style={{ listStyleType: 'none', fontSize: '18px' }}>A modern CSS framework for faster and better responsive web pages</li>
                        <button style={{ backgroundColor: 'grey', border: 'none', height: '40px', width: '150px', color: 'white', marginTop: '20px' }}>LEARN W3.CSS</button>
                    </div>
                </div>
                <div className="cols-lg-4">
                    <div style={{ textAlign: 'center', width: '400px', marginTop: '20px' }}>
                        <li style={{ listStyleType: 'none', fontSize: '24px' }}>COLORPICKER</li>
                        <img src="https://www.w3schools.com/images/colorpicker.png" alt="colorpicker" style={{ marginTop: '20px' }} />
                    </div>
                </div>
                <div className="cols-lg-4">
                    <div style={{ textAlign: 'center', width: '300px', marginTop: '20px' }}>
                        <li style={{ listStyleType: 'none', fontSize: '24px' }}>BOOTSTRAP</li>
                        <li style={{ listStyleType: 'none', fontSize: '18px' }}>A CSS framework for designing better web pages</li>
                        <button style={{ backgroundColor: 'grey', border: 'none', height: '40px', width: '200px', color: 'white', marginTop: '20px' }}>LEARN BOOTSTRAP</button>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="cols-lg-12" style={{ marginTop: '50px', backgroundColor: 'grey', textAlign: 'center', width: '100%', height: '300px' }}>
                    <li style={{ listStyleType: 'none', fontSize: '24px', color: 'white', marginTop: '40px' }}>Exercises</li>
                    <li style={{ listStyleType: 'none', fontSize: '18px', color: 'white', marginTop: '10px' }}>Test yourself with Exercises</li>
                    <div style={{ marginTop: '20px' }}>
                        <button style={{ border: 'none', backgroundColor: 'green', color: 'white', height: '40px', width: '270px', marginLeft: '20px' }}>HTML Exercises</button>
                        <button style={{ border: 'none', backgroundColor: 'green', color: 'white', height: '40px', width: '270px', marginLeft: '20px' }}>CSS Exercises</button>
                        <button style={{ border: 'none', backgroundColor: 'green', color: 'white', height: '40px', width: '270px', marginLeft: '20px' }}>Javascript Exercises</button>
                    </div>
                    <div style={{ marginTop: '20px' }}>
                        <button style={{ border: 'none', backgroundColor: 'green', color: 'white', height: '40px', width: '200px', marginLeft: '20px' }}>SQL</button>
                        <button style={{ border: 'none', backgroundColor: 'green', color: 'white', height: '40px', width: '200px', marginLeft: '20px' }}>PHP</button>
                        <button style={{ border: 'none', backgroundColor: 'green', color: 'white', height: '40px', width: '200px', marginLeft: '20px' }}>Python</button>
                        <button style={{ border: 'none', backgroundColor: 'green', color: 'white', height: '40px', width: '200px', marginLeft: '20px' }}>jQuery</button>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="cols-lg-12" style={{ backgroundColor: '#f2f2f2', height: '200px', width: '100%', textAlign: 'center', height: '650px' }}>
                    <div style={{ marginTop: '20px' }}>
                        <li style={{ listStyleType: 'none', fontSize: '40px' }}>Web Templates</li>
                        <li style={{ listStyleType: 'none', fontSize: '20px' }}>Browse ur selection of <b>free</b> responsive HTML Templates</li>
                        <img src="https://www.w3schools.com/w3css_templates.jpg" alt="web templates" height="400px" width="900px" style={{ marginTop: '20px' }} />
                    </div>
                    <div style={{ marginTop: '30px' }}>
                        <button style={{ border: 'none', backgroundColor: 'grey', color: 'white', height: '40px', width: '200px', }}>BROWSE TEMPLATES</button>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="cols-lg-12" style={{ width: '100%', }}>
                    <div style={{ marginTop: '30px', textAlign: 'center' }}>
                        <li style={{ listStyleType: 'none', fontSize: '40px' }}>How To Section</li>
                        <li style={{ listStyleType: 'none', fontSize: '20px' }}>Code snippets for HTML, CSS and JavaScript</li>
                        <button style={{ backgroundColor: 'grey', marginTop: '20px', height: '40px', width: '150px', color: 'white', border: 'none' }}>LEARN HOW TO</button>
                    </div>
                    <div style={{ textAlign: 'center', position: 'relative', marginTop: '30px', backgroundColor: 'black' }}>
                        <img src="https://www.w3schools.com/getcertified.jpg" alt="getcertified" height="400px" width="800px" />
                        <div style={{ position: 'absolute', backgroundColor: 'black', color: 'white', opacity: '0.7', textAlign: 'center', top: '5px', left: '0px', width: '100%', height: '120px' }}>
                            <li style={{ listStyleType: 'none', marginTop: '10px', fontSize: '34px' }}>Web Certificates</li>
                            <li style={{ listStyleType: 'none', marginTop: '10px', fontSize: '18px' }}>Get Certified in HTML , CSS and JavaScript</li>
                        </div>
                        <div style={{ position: 'absolute', top: '50%', left: '50%' }}><button style={{ backgroundColor: 'green', color: 'white', height: '40px', width: '150px', border: 'none' }}>Get Certified>></button></div>
                    </div>
                    <div style={{ backgroundColor: 'grey', height: '400px' }}>
                        <div>
                            <button style={{ backgroundColor: '#f2f2f2', height: '40px', width: '200px', marginLeft: '10px', marginTop: '50px', border: 'none' }}>WEB CERTIFICATES</button>
                            <button style={{ backgroundColor: '#f2f2f2', height: '40px', width: '150px', marginLeft: '20px', marginTop: '50px', border: 'none' }}>QUIZZES</button>
                            <button style={{ backgroundColor: '#f2f2f2', height: '40px', width: '150px', marginLeft: '20px', marginTop: '50px', border: 'none', }}>EXERCISES</button>
                            <li style={{ listStyleType: 'none', float: 'right', marginTop: '50px', color: 'white', marginRight: '20px', fontSize: '18px' }}>ABOUT</li>
                            <li style={{ listStyleType: 'none', float: 'right', marginTop: '50px', color: 'white', marginRight: '10px', fontSize: '18px' }}>|</li>
                            <li style={{ listStyleType: 'none', float: 'right', marginTop: '50px', color: 'white', marginRight: '10px', fontSize: '18px' }}>FORUM</li>

                        </div>
                        <div style={{ textAlign: 'center', marginTop: '20px', color: 'white', lineHeight: '1', }}>
                            <p>w3Schools is optimized for learning, testing and training.
                                        Examples might be simplified to improve reading and basic understanding.</p>
                            <p>Tutorials, references, and examples are constantly reviewed to avoid errors,but we cannot warrant full correctness of all content.</p>
                            <p>While using this site, you agree to have read and accepted our <u>terms of use, </u> <u>cookie and privacy policy.</u></p>
                            <p><u>Copyright 1999-2019</u> by Refsnes Data. All Rights Reserved.</p>
                        </div>

                        <div style={{ textAlign: 'center', marginTop: '30px' }}>
                            <i class="far fa-thumbs-up fa-4x" style={{ color: 'white' }}></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row" style={{ height: '200px', width: '100%' }}>

            </div>
        </div>

    );
}