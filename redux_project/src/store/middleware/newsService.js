import request from 'superagent';
import * as newsActions from '../actions/newsActions';
import * as allActions from '../actions/actionConstants';

const newsService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_NEWS:
            request.get('http://13.229.176.226:8001/api/news/recent')
                .then(res => {
                    const data = JSON.parse(res.text);
                    next(newsActions.receiveNews(data));
                })
                .catch(err => {
                    next({ type: 'FETCH_NEWS_DATA_ERROR', err });
                });
            break;
        case allActions.FETCH_NEWS_BY_ID:
            request.get('http://13.229.176.226:8001/api/news' + `/${action.payload.id}`)
                .then(res => {
                    const data = JSON.parse(res.text);
                    next(newsActions.receiveNewsByID(data));
                })
                .catch(err => {
                    next({ type: 'FETCH_NEWS_BY_ID_DATA_ERROR', err });
                });
            break;
        default:break;
    };
};

export default newsService;