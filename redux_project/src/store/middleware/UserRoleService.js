import * as allActions from '../actions/actionConstants';
import * as userRoleActions from '../actions/UserRoleActions';
import request from 'superagent';

const userRoleService = (store) => next => action => {
    next(action)
    console.log('user role service');
    switch(action.type){
        case allActions.FETCH_USER:
        console.log('fetch user service')
        request.get('http://13.250.235.137:8050/api/user')
        .set('Authorization',('Bearer ' + localStorage.getItem('jwt-token')))
        .then(res => {
            console.log('success');
            const data = JSON.parse(res.text);
            next(userRoleActions.receiveUser(data));
        })
        .catch(err => {
            console.log('error');
            next({ type: 'FETCH_USER_ROLE_ERROR',err});
        });
        break;
        default : break;
    }
}
export default userRoleService;