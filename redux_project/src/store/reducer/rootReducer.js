import {combineReducers}  from 'redux';
import counterReducer from './counterReducer';
import newsReducer from './newsReducer';
import authReducer from './authReducer';
import userRoleReducer from './userRoleReducer';

const rootReducer = combineReducers({
    counterReducer,
    newsReducer,
    authReducer,
    userRoleReducer,
});

export default rootReducer;