import * as allActions from '../actions/actionConstants';

const initialState = {
    role :'',
}

export default function userRoleReducer(state = initialState, action){
    switch (action.type) {
        case allActions.FETCH_USER:
        console.log('reducer fetch')
        return action;
        case allActions.RECEIVE_USER:
        console.log('receive reducer')
            return {
                ...state,
                role : action.payload.user.role,
            };
        default: return state;
    }
}