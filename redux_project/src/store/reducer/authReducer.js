import * as allActions from '../actions/actionConstants';

const initailState = {
    is_logged_in : false,
    statuscode : ''
}

export default function authReader(state = initailState, action){
    switch (action.type) {
        case allActions.LOGIN_USER_SUCCESS:
            return {
                ...state,
                is_logged_in: true
            }
        case allActions.LOGIN_USER_DATA_ERROR:
            return{
                is_logged_in:false,
                statuscode : action.payload
            }
        default: return state;
    }
}