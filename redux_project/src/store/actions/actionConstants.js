export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
/**
 * action constants for news scenario
 */

export const FETCH_NEWS = 'FETCH_NEWS';
export const RECEIVE_NEWS = 'RECEIVE_NEWS';

/**
 * action constants for search news by id
 */

export const FETCH_NEWS_BY_ID = 'FETCH_NEWS_BY_ID';
export const RECEIVE_NEWS_BY_ID = 'RECEIVE_NEWS_BY_ID';

/**
 * Action constants for Login secenario
 */

 export const DO_LOGIN_USER = 'DO_LOGIN_USER';
 export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';

 export const LOGOUT_USER = 'LOGOUT_USER';


 /**
  * Action Constants to know Users Role
  */

  export const FETCH_USER = 'FETCH_USER';
  export const RECEIVE_USER = 'RECEIVE_USER';

  export const LOGIN_USER_DATA_ERROR = 'LOGIN_USER_DATA_ERROR';