import * as allActions from '../actions/actionConstants';

export function fetchUser(){
    console.log('fetch user');
    return {type:allActions.FETCH_USER,payload:{}};
}

export function receiveUser(data){
    console.log('received role');
    return {type:allActions.RECEIVE_USER,payload:data};
}