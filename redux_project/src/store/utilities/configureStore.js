import { createStore, applyMiddleware,compose } from 'redux';
import rootReducer from '../reducer/rootReducer';
import newsMiddleware from '../middleware/newsService'
import authMiddleware from '../middleware/authService';
import userRoleService from '../middleware/UserRoleService';


export default function configureStore(){
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            newsMiddleware,
            authMiddleware,
            userRoleService
        ))
    );
};