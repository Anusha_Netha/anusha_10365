import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as newsActions from '../store/actions/newsActions'
import '../assets/css/news.css';

class Body extends Component {
    componentDidMount() {
        this.props.newsActions.fetchNews();
    }
    render() {
        return (
            <div className="container" >
                <div className="row">
                    <div className="col-md-12 col-sm-12 card" id="background">
                        <h1 id="information">Information about Cancer</h1>
                        <p id="details">Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 col-sm-12">
                        <table>
                            <tr><td id="news">News</td></tr>
                            <tr><td>NGOs/NPOs</td></tr>
                            <tr><td>Support group</td></tr>
                            <tr><td>Online help</td></tr>
                        </table>
                    </div>
                    <div className="col-md-9 col-sm-12">
                        <h3 className="news1">News</h3>
                        <a href="#" className="showall">Show all</a><br /><br />
                        <hr></hr>
                        <div>
                            {
                                (this.props.newsItems)
                                    ? <div>
                                        {this.props.newsItems.map(item => (
                                            <div key={item._id} className="body">
                                                <img src={item.image} alt="news" className="newsimage" />
                                                <div>
                                                    <p className="comment"><i className="fa fa-comments" >&nbsp;{item.comments}</i></p>
                                                    <p className="like"><i className="fa fa-thumbs-up">&nbsp;{item.likes.likes}</i></p>
                                                </div>
                                                <div className="newsLine">
                                                    <a href={`/news/${item._id}`} className="heading">{item.heading}</a>
                                                    <p className="brief">{item.brief}</p>
                                                </div>

                                            </div>
                                        ))}
                                    </div>
                                    : <div>
                                        loading...
                                </div>
                            }
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Body);