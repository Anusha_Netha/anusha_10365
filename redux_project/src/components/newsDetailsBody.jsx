import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as newsActions from '../store/actions/newsActions';

class NewsDetailsBody extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            selectedNews:{}
         }
    }
    componentWillMount(){
        this.props.newsActions.fetchNewsByID(this.props.match.params.id);
    }
    componentWillReceiveProps(newProps){
        if(newProps.newsItem1){
            this.setState({
                selectedNews:newProps.newsItem1,
            })
        }
    }
    render() { 
        return ( 
            <div>
                {this.state.selectedNews.body}<br/>
                PublishDate:{this.state.selectedNews.timestamp} 
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        newsItem1: state.newsReducer.newsItem,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsDetailsBody);