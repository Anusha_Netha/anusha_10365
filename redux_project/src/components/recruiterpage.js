import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import '../assets/css/recruiter.css';

class RecruiterPage extends Component {
    constructor(props){
        super(props);
    this.state = { 
        valid : false
     }
    }
    DoLogout = e =>{
        localStorage.removeItem("jwt-token");
        this.setState ({valid : true});
        window.location.reload();
    }
    render() { 
        if (this.state.valid === true){
            return (
                <Redirect to = '/login' />
            )
        }
        return ( 
            <div id="recruiterpage">
                <p className="role">Hello Recruiter</p>
                <br></br>
                <button onClick={this.DoLogout} className="logout">Logout</button>
            </div>
         );
    }
}
 
export default RecruiterPage;