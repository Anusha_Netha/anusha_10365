import React, { Component } from 'react';
import '../assets/css/news.css';
import Navigation from './Navigation';
import Body from './Body';
import Footer from './Footer';

class News extends Component {
   
    render() {
        return (
                <div>
                    <Navigation/>
                    <Body/>
                    <Footer/>          
                </div>
        );
    }
}


export default News;