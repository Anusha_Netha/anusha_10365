import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import '../assets/css/user.css';

class UserPage extends Component {
    constructor(props){
        super(props);
    this.state = { 
        valid : false
     }
    }
    DoLogout = e =>{
        localStorage.removeItem("jwt-token");
        this.setState ({valid : true});
        window.location.reload();
    }
    render() { 
        if (this.state.valid === true){
            return (
                <Redirect to = '/login' />
            )
        }
        return ( 
            <div id="userpage">
                <p className="role">Hello User</p>
                
                <button onClick={this.DoLogout} className="logout">Logout</button>
            </div>
         );
    }
}

export default UserPage;