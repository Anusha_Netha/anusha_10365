import React, { Component } from 'react';
// import {connect} from 'react-redux';
// import {bindActionCreators} from 'redux';
// import * as newsActions from '../store/actions/newsActions';
import Navigation from './Navigation';
import '../assets/css/newsDetails.css';
import NewsDetailsBody from '../components/newsDetailsBody';

class NewsDetails extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = { 
    //         selectedNews:{}
    //      }
    // }
    // componentWillMount(){
    //     this.props.newsActions.fetchNewsByID(this.props.match.params.id);
    // }
    // componentWillReceiveProps(newProps){
    //     if(newProps.newsItem1){
    //         this.setState({
    //             selectedNews:newProps.newsItem1,
    //         })
    //     }
    // }
    render() { 
        return ( 
            <React.Fragment>
                <div>
                    <Navigation/>
                    <NewsDetailsBody/>
                    {/* {this.state.selectedNews.body}<br/>
                    PublishDate:{this.state.selectedNews.timestamp} */}
                    <div className="container">
                        <div className="heading">
                            <a className="information" href="#">Information</a>&nbsp;/&nbsp;
                            <a className="information" href="#">News</a>
                            <hr></hr>
                        </div>
                        <div>
                            <h1 >{this.state.selectedNews.heading}</h1>
                            <p>{this.state.selectedNews.timestamp}</p>
                        </div>
                    </div>
                </div>
            </React.Fragment>  
         );
    }
}

// function mapStateToProps(state) {
//     return {
//         newsItem1: state.newsReducer.newsItem,
//     };
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         newsActions: bindActionCreators(newsActions, dispatch),
//     };
// }
// export default connect(mapStateToProps, mapDispatchToProps)(NewsDetails);
export default NewsDetails;