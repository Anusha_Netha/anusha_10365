import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import '../assets/css/admin.css';

class Admin extends Component {
    constructor(props){
        super(props);
    this.state = { 
        valid : false
     }
    }
    DoLogout = e =>{
        localStorage.removeItem("jwt-token");
        this.setState ({valid : true});
        window.location.reload();
    }
    render() { 
        if (this.state.valid === true){
            return (
                <Redirect to = '/login' />
            )
        }
        return ( 
            <div id="adminpage">
                <p className="role">Hello Admin</p>
                <br></br>
                <button onClick={this.DoLogout} className="logout">Logout</button>
            </div>
         );
    }
}
 
export default Admin;