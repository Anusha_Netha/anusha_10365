import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {  bindActionCreators } from 'redux';
import * as UserRoleActions from '../store/actions/UserRoleActions'

class DashBoard extends Component {
    componentDidMount(){
        this.props.UserRoleActions.fetchUser();
    }
    render() { 
        if(this.props.UserRole === 'user'){   
            return <Redirect to = '/userspage'/>    
        }
        else if(this.props.UserRole === 'admin'){
            return <Redirect to = '/adminpage'/>
        }
        else if(this.props.UserRole === 'recruiter'){
            return <Redirect to = '/recruiterpage'/>
        }
        else{
            return (
                <div ><span class="spinner-border"></span>Loading...</div>
            );
        }
    }
}

function mapStateToProps(state){
    console.log(state.authReducer.statuscode)
    return {
        UserRole: state.userRoleReducer.role,
    };
}

function mapDispatchToProps(dispatch){
    return{
        UserRoleActions: bindActionCreators(UserRoleActions, dispatch),
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(DashBoard);