import React, { Component } from 'react';
import '../assets/css/news.css';

export default function Navigation() {
  return (
    <nav className="navbar navbar-expand-lg ">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars" id="icon-bar"></i>
      </button>
      <a className="navbar-brand" href="#">AidForCancer</a>
      <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li className="nav-item" style={{ marginLeft: 620 }}>
            <a className="nav-link" href="#" >Information  </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">Opinions</a>
          </li>
          <li className="nav-item">
            <a className="nav-link " href="#">Forum</a>
          </li>
          <li className="nav-item">
            <a className="nav-link " href="#">Volunteer</a>
          </li>
          <li className="nav-item">
            <a className="nav-link " href="#">Seek Help</a>
          </li>
          <li className="nav-item">
            <button className="nav-link" id="sign_in">Sign In</button>
          </li>
        </ul>
      </div>
    </nav>

  );
}