import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {  bindActionCreators } from 'redux';
import * as authActions from '../store/actions/authActions';
import '../assets/css/Login.css';
import * as UserRoleActions from '../store/actions/UserRoleActions'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            password:'',
            isSubmitted : false,

        };
        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();
    }
    doLogin = e => {
        this.props.authActions.doLoginUser({
            email : this.state.email,
            password : this.state.password
        })
        this.setState({
            isSubmitted:true
        })
    }
    render() {
        if(this.state.isSubmitted){
            // console.log(localStorage.getItem('jwt-token'))
            if(this.props.isLoggedIn === true){
                window.location.reload();
                return <Redirect to = '/dashboard'/>
            }
            else{
                if(this.props.statusCode == -1){
                    alert('incorrect email or password');
                    this.setState({isSubmitted:false})
                }
                else if(this.props.statusCode == -2){
                    alert('incorrect password format')
                    this.setState({isSubmitted:false})
                }
                else if(this.props.statusCode == -3){
                    alert('incorrect password')
                    this.setState({isSubmitted:false})
                }
            }
        }
        return (
            <div className="login">
                <div>
                    <div id="form">
                        <label>Email ID:&nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="email" type="text" name="email" placeholder="Enter your EmailID" onChange={this.handleChange} value={this.state.email} />
                        </label><br />
                        <label>Password:&nbsp;&nbsp;
                        <input id="password" type="password" name="password" placeholder="Enter your Password" onChange={this.handleChange} value={this.state.password} />
                        </label><br />
                        <button onClick={this.doLogin} class="submit">Submit</button>
                    </div>
                </div>

            </div>
        );
    }
}

function mapStateToProps(state){
    console.log(state.authReducer.statuscode)
    return {
        isLoggedIn : state.authReducer.is_logged_in,
        statusCode : state.authReducer.statuscode,
    };
}

function mapDispatchToProps(dispatch){
    return{
        authActions : bindActionCreators(authActions,dispatch),
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);