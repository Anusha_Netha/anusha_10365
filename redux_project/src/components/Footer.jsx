import React, { Component } from 'react';
import '../assets/css/news.css';

class Footer extends Component {
    render() {
        return (
            <div style={{ marginTop: 10 }}>
                <div className="row">
                    <div className="  footer col-md-6 col-sm-12 ">
                        <p className="contact">
                            <a href="#" className="footlink" >Contact Us</a>
                        </p>
                        <button className="abusebtn">Report Abuse</button>
                    </div>
                    <div className=" footer col-md-6 col-sm-12" >
                        <p className="About">
                            <a href="#" className="footlink">About Us</a>
                        </p>
                        <p className="privacy">
                            <a href="#" className="footlink">  Privacy policy</a>
                        </p>
                        <p className="privacy">
                            <a href="#" className="footlink"> Disclaimer</a>
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div className="copyright col-md-12 col-sm-12" >
                        <p> <b> © 2019 AidForCancer</b></p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;