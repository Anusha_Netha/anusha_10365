import React, { Component } from 'react';
import {BrowserRouter,Switch,Route } from 'react-router-dom';
import News from './components/news';
import Home from './components/Home';
import NewsDetails from './components/newsDetails';
import Login from './components/Login';
import UserPage from './components/User.js';
import RecruiterPage from './components/recruiterpage';
import Admin from './components/admin';
import DashBoard from './components/DashBoard';

class Routes extends Component {
    render() { 
        return ( 
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/news" component={News} exact/>
                    <Route path={'/news/:id'} component={NewsDetails}/>
                    <Route path={`/login`} component={Login}/>
                    <Route path={`/dashboard`} component={DashBoard}/>
                    <Route path={`/userspage`} component={UserPage}/>
                    <Route path={`/recruiterpage`} component={RecruiterPage}/>
                    <Route path={`/adminpage`} component={Admin}/>
                </Switch>
            </BrowserRouter>
         );
    }
}
 
export default Routes;