import React, { Component } from 'react';

class Register extends Component{
    constructor(props){
        super(props);
        this.state = {
            username : '',
            fav : ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({[event.target.name]:event.target.value});
    }
    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.username +" " + 'your favourite fruit:' + this.state.fav);
        
        event.preventDefault();
    }
    render(){
        return(
            <div>
                <h3>Registration Form</h3>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Name:
                        <input type="text" name="username" value={this.state.username} onChange={this.handleChange} /><br></br>
                    </label>
                    <label>
                        Pick your favorite flavor:
                        <select name="fav" value={this.state.fav} onChange={this.handleChange}>
                            <option selected></option>
                            <option value="grapefruit">Grapefruit</option>
                            <option value="lime">Lime</option>
                            <option value="coconut">Coconut</option>
                            <option value="mango">Mango</option>
                        </select><br></br>
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}

export default Register;