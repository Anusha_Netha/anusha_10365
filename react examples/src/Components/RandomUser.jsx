import React, { Component } from 'react';

class RandomUser extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            userDetails : []
         };
    }
    componentDidMount(){
        console.log("inside did mount");
        fetch('https://randomuser.me/api/?result=500')
        .then(res => {
            return res.json();
        }).then(data => {
            let users = data.results.map(pic =>{
                return(
                    <div key={pic.results}>
                        <img src={pic.picture.large}/>
                    </div>
                )
            })
            this.setState({
                userDetails : users
            })
        })
    }
    render() { 
        return ( 
            <div>{this.state.userDetails}</div>
         );
    }
}
 
export default RandomUser;