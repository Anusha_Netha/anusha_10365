import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Clock from './Components/clock';
import Registration from './Components/register';
import Routes from './route';
import CustomLinkExample from './Components/Example';
import FetchUser from './Components/FetchUser';
import FetchPost from './Components/FetchPost';
import RandomUser from './Components/RandomUser';

class App extends Component {
  render() {
    return (
      // <Clock/>
      // <Registration/>
      <Routes/>
      // <CustomLinkExample/>
      // <FetchUser/>
      // <FetchPost/>
      // <RandomUser/>
    );
  }
}

export default App;