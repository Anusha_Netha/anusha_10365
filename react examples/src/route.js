import React from 'react';
import Clock from './Components/clock';
import Register from './Components/register';
import {BrowserRouter,Route} from 'react-router-dom';
import Home from './Components/Home';
import Cars from './Components/Car';
import CarDetail from './Components/CarDetails';

const Routes = () => (
    <BrowserRouter>
        <switch>
            <Route exact path={"/"} component={Home}/>
            <Route path={'/clock'} component={Clock}/>
            <Route path={"/register"} component={Register}/>
            <Route path={"/cars"} component={Cars} />
            <Route path={'/cars/:id'} component={CarDetail}/>
        </switch>
    </BrowserRouter>
)
export default Routes;