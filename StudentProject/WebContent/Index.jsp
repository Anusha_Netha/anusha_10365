<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
table{
border : border-collapse;
margin-top : 30px;
margin-left : 500px;
border-spacing : 10px;
background-color : #b56969;
width : 300px;
}
table{
border : 1px solid black;
}
a{
text-decoration : none;
}
</style>
</head>
<body bgcolor="#e8edf3">
<% String name = (String)session.getAttribute("name");
if(name == null){ response.sendRedirect("Authorized.html");}
else{%>
<h4>Hello <%= name %></h4>
<form>
<table>
<tr><td align="center">STUDENT PORTAL</td></tr>
<tr><td align="center"><a href="Insert.html">Insert A Student</a></td></tr>
<tr><td align="center"><a href="DisplayById.html">Get a Student By ID</a></td></tr>
<tr><td align="center"><a href="DisplayAllStudents">Get All Student</a></td></tr>
<tr><td align="center"><a href="UpdateById.html">Update a Student by ID</a></td></tr>
<tr><td align="center"><a href="delete.html">Delete a Student by ID</a></td></tr>
</table>
</form>
<%} %>
</body>
</html>