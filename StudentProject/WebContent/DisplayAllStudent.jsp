<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
a{
text-decoration : none;
}
</style>
</head>
<body bgcolor="#e8edf3">

<table border="1">
<tr><th>ID</th><th>NAME</th><th>AGE</th></tr>
<c:forEach var="student" items="${studentList}">
<tr><td>${student.id}</td><td>${student.name}</td><td>${student.age}</td>
</c:forEach>
</table>
<br>
<a href="Index.jsp">Go Back</a>

</body>
</html>