package com.student.service;

import java.util.List;

import com.student.bean.Student;
import com.student.dao.StudentDAO;
import com.student.dao.StudentDAOImpl;


/**
 * this class is used to provide the services to access the database
 * @author IMVIZAG
 *
 */
public class StudentServiceImpl implements StudentService{

	/**
	 * this method is used to insert a student using createStudent()
	 */
	@Override
	public boolean insertStudent(Student student) {
		// TODO Auto-generated method stub
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.createStudent(student);
		
		return result;
	}

	/**
	 * this method is used to search a student by searchById()
	 */
	@Override
	public Student findById(int id) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		Student student = studentDAO.searchById(id);
		return student;
	}

	/**
	 * this method is used to display all students by getAllStudents()
	 */
	@Override
	public List<Student> fetchAllStudents() {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}

	/**
	 * this method is used to update a student's name by their id using changeStudent()
	 */
	@Override
	public boolean updateStudent(int id,String name) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.changeStudent(id,name);
		return result;
	}

	/**
	 * this method is used to delete a student by their id using deleteById()
	 */
	@Override
	public boolean deleteById(int id) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.deleteById(id);
		return result;
	}

	
	
	

}














