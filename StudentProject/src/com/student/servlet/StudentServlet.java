package com.student.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.bean.Student;
import com.student.service.StudentService;
import com.student.service.StudentServiceImpl;



/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("studentname");
		int studentAge = Integer.parseInt(request.getParameter("age"));
			
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
        request.setAttribute("student",student);
        
        @SuppressWarnings("unused")
		HttpSession session =request.getSession(false);
        
        RequestDispatcher rd = request.getRequestDispatcher("Insert.jsp");
	
		StudentService service = new StudentServiceImpl();
		boolean result = service.insertStudent(student);
		if(result) {
			rd.forward(request, response);
		}else {
			response.sendRedirect("Error1.html");
		}
		
//				
//		PrintWriter out = response.getWriter();
//		
//		out.print("<html><body bgcolor='#e8edf3'>");
//		
//		if(result) {
//			out.print("<h2>Student Saved</h2>");
//		}
//		else {
//			out.print("<h2>Something wrong</h2>");
//		}
//		
//		out.print("</body></html>");
//		out.close();
//		
		
	}

}

