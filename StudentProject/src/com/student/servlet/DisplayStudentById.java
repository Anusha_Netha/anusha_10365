package com.student.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.bean.Student;
import com.student.service.StudentService;
import com.student.service.StudentServiceImpl;


/**
 * Servlet implementation class DisplayStudentById
 */
@WebServlet("/DisplayStudentById")
public class DisplayStudentById extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DisplayStudentById() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		@SuppressWarnings("unused")
		HttpSession session = request.getSession(false);
		
		int id = Integer.parseInt(request.getParameter("id"));
		StudentService service = new StudentServiceImpl();
		Student student = service.findById(id);
		RequestDispatcher rd = request.getRequestDispatcher("DisplayById.jsp");
		request.setAttribute("student",student);
		if(student != null) {
			rd.forward(request, response);
		}
		else {
			response.sendRedirect("Error1.html");
		}
		
//		PrintWriter out = response.getWriter();
//		out.print("<html><body bgcolor='#e8edf3'>");
//		if(student != null) {
//		out.println("<table border='1'>");
//		out.println("<tr><th>ID</th><th>Name</th><th>Age</th></tr>");
//		out.println("<tr><td>" + student.getId() + "</td>");
//		out.println("<td>" + student.getName() + "</td>");
//		out.println("<td>" + student.getAge() + "</td></tr>");
//		out.println("</table>");
//		}
//		else {
//			out.println("Sorry!!!We dont have records");
//		}
//		out.print("</body></html>");
//		out.close();

	}

}
