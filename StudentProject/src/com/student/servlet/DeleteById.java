package com.student.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.service.StudentService;
import com.student.service.StudentServiceImpl;


/**
 * Servlet implementation class DeleteById
 */
@WebServlet("/DeleteById")
public class DeleteById extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteById() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int studentId = Integer.parseInt(request.getParameter("id"));
     
		@SuppressWarnings("unused")
		HttpSession session = request.getSession(false);
		StudentService service = new StudentServiceImpl();
		boolean result = service.deleteById(studentId);

		RequestDispatcher rd = request.getRequestDispatcher("delete.jsp");
		if(result) {
			rd.forward(request, response);
		}
		else {
			response.sendRedirect("Error1.html");
		}
//		PrintWriter out = response.getWriter();
//		
//		out.print("<html><body bgcolor='#e8edf3'>");
//		
//		if(result) {
//			out.print("<h2>Delete Successfull</h2>");
//		}
//		else {
//			out.print("<h2>Something wrong</h2>");
//		}
//		
//		out.print("</body></html>");
//		out.close();	
	}

}
