package com.student.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.service.StudentService;
import com.student.service.StudentServiceImpl;


/**
 * Servlet implementation class UpdateStudentByName
 */
@WebServlet("/UpdateStudentByName")
public class UpdateStudentByName extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStudentByName() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("username");
		
		@SuppressWarnings("unused")
		HttpSession session = request.getSession(false);
		request.setAttribute("studentName",studentName);
		
		StudentService service = new StudentServiceImpl();
		boolean result = service.updateStudent(studentId,studentName);
		RequestDispatcher rd = request.getRequestDispatcher("Update.jsp");
		if(result) {
			rd.forward(request, response);
		}
		else {
			response.sendRedirect("Error1.html");
		}
		
//		PrintWriter out = response.getWriter();
//		
//		out.print("<html><body bgcolor='#e8edf3'>");
//		
//		if(result) {
//			out.print("<h2>Updated Successfully</h2>");
//		}
//		else {
//			out.print("<h2>Something wrong</h2>");
//		}
//		
//		out.print("</body></html>");
//		out.close();	
	}

}
