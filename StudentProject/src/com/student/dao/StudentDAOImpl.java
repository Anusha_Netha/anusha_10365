package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.student.bean.Student;
import com.student.util.DBUtil;



/**
 * This class is used to connect to student database
 * @author IMVIZAG
 *
 */


public class StudentDAOImpl implements StudentDAO {

	/**
	 * This method is used to insert a student into database
	 */
	@Override
	public boolean createStudent(Student student) {
		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "insert into student_tbl values( ?, ?, ?)";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	/**
	 * this method is used to search a student by their id
	 */
	@Override
	public Student searchById(int id) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		String sql = "select * from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while(rs.next()) {
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				
			}
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return st;
	}

	/**
	 * this method is used to display all the students in the database
	 */
	@Override
	public List<Student> getAllStudents() {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		List <Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_tbl";
		try {
			con = DBUtil.getCon();
			//System.out.println(con);
			stmt= con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				studentList.add(st);
			}
		}
		catch (SQLException e){
			
		}
		finally {
//			try {
//				con.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
		}
		
		return studentList;
	}

	/**
	 * this method is used to update a student's name by their id
	 */
	@Override
	public boolean changeStudent(int id,String name) {
		
		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "update student_tbl set name = ? where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, id);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;	
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
		
	}
	
	/**
	 * this method is used to delete a student by their id 
	 */

	@Override
	public boolean deleteById(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "delete from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;	
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}



}











