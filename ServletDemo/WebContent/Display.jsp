<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import = "java.util.List" %>
    <%@ page  import = "java.util.Iterator"  %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
String name = request.getParameter("name");
String email = request.getParameter("email");
List<String> list = (List<String>) request.getAttribute("courses");
%>
<h2> Hello <%= name %> </h2> 
<h2> Email <%= email %></h2>

<table>
<tr><th>Courses</th></tr>
<%
Iterator<String> i = list.iterator();
while(i.hasNext()){
	String course = i.next();
%>
<tr><td><%= course %></td></tr>

<%
}
%>
</table>
</body>
</html>