package com.servletdemo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SourceServlet1
 */
@WebServlet("/SourceServlet1")
public class SourceServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("name");
		String pass1 = request.getParameter("pwd1");
		String pass2 = request.getParameter("pwd2");
		String email = request.getParameter("email");
		
		HttpSession session = request.getSession();
		session.setAttribute("myemail",email);
		session.setMaxInactiveInterval(20);
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		
		pw.println("<htm><body>");
		
		if(pass1.equals(pass2)) {
			pw.println("Hello  "+name+"<br/>");
			pw.println("<a href='DisplayServlet'>Click here to see your E-Mail Id</a>");
	
		}
		else {
			response.sendRedirect("Error.html");
		}
		pw.println("</body></html>");
		pw.close();
	}

}
