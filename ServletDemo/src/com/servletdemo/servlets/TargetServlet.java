package com.servletdemo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TargetServlet
 */
@WebServlet("/TargetServlet")
public class TargetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//String name = request.getParameter("name");
		String email = request.getParameter("email");
		List<String> course = new ArrayList<String>();
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><body>");
		//pw.println("NAME = "+name+"<br>");
		pw.println("E-MAIL = "+email+"<br>");
		
		course = (List<String>) request.getAttribute("courses");
		Iterator<String> i = course.iterator();
		pw.println("<br/><br/>Courses:<br/>");
		while(i.hasNext()) {
			String courses = i.next();
			pw.println(courses);
			pw.println("<br/>");
		}
		
		pw.println("</body></html>");
	}

}
