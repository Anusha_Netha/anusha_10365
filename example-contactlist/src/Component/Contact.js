import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as ConatctAction from '../Store/actions/contactAction';

class ContactList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(e) {
        this.setState({
            name: e.target.value
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.name.length === 0) {
            alert('name should not be empty');
        }
        else {

            // console.log(this.state.name);
            let contact = {
                name: this.state.name,
            }
            this.props.createContact(contact);
            this.state.name = "";
        }
    }
    listView(data, index) {
        return (
            <div>
                <div className="row">
                    <div className="col-md-10">
                        <li key={index} className="list-group-item clearfix">{data.name}</li>
                    </div>
                    <div className="col-md-2">
                        <button onClick={(e) => this.deleteContact(e, index)} className="btn btn-danger mt-2">Remove</button>
                    </div>
                </div>
                <hr />
            </div>
        )
    }

    deleteContact(e, index) {
        e.preventDefault();
        this.props.deleteContact(index);
    }

    render() {
        return (
            <div>
                <div className="container">
                    <h1>Contacts List</h1>
                    <hr />
                    <div>
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" name="name" onChange={this.handleChange} className='form-control '
                                value={this.state.name} />
                            <input type="submit" value="ADD CONTACT" className="btn btn-success mt-2" />
                        </form>
                        <hr />
                        {
                            <ul className="list-group">
                                {this.props.contacts.map((contact, i) => this.listView(contact, i))}
                            </ul>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        createContact: contact => dispatch(ConatctAction.createContact(contact)),
        deleteContact: index => dispatch(ConatctAction.removeContact(index))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);