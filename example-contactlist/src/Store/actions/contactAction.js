import * as allActions from '../actions/actionConstants';

export function createContact(contact) {
    console.log("action ")
    return {
        type:allActions.CREATE_NEW_CONTACT,
        contact:contact
    };
}

export function removeContact(id){
    console.log('remove action')
    return {
        type:allActions.REMOVE_CONTACT,
        id:id,
    }
}